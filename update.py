#!/usr/bin/python

import sys
import urllib2
import logging
import os
import ConfigParser

def main():
	filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)))
	logfile = os.path.join(filepath, 'update.log')
	logging.basicConfig(filename=logfile, level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

	# Read the config file:
	Config = ConfigParser.ConfigParser()
	Config.read(os.path.join(filepath, 'config.cfg'))
	username = Config.get('credentials', 'username')
	password = Config.get('credentials', 'password')

	url = "https://api.unblock-us.com/login"

	# Attempt to update:
	request = urllib2.urlopen(url + "?" + username + ":" + password)
	response = request.read()	
	request.close()

	# Parse the response:
	if (response == "active"):
		logging.info("IP address is active.")
		exit(0)
	elif (response == "bad_password"):
		logging.info("Wrong username or password.")
		exit(1)
	elif (response == "not_found"):
		logging.info("Username not found.")
		exit(1)
	else:
		logging.info("Unknown error. Check api url or documentantion. Response is:\n%s" % response)
		exit(1)

if __name__ == "__main__":
	main()
