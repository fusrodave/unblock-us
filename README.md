# README #

### What is this repository for? ###

* https://www.unblock-us.com matches accounts to IP.
* IP can change depending on your ISP.
* This is updated by submitting a change to unblock-us through the browser when you attempt to resolve a connection.
* This repo scripts this automagically.

### How do I get set up? ###

* Copy sample_config.cfg to config.cfg.
* Update username / password.
* Add script to cron / other scheduler.